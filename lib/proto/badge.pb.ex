defmodule Badgeapi.CreateBadgeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: String.t(),
          order: String.t(),
          video_url: String.t(),
          image: String.t()
        }

  defstruct [:name, :order, :video_url, :image]

  field :name, 1, type: :string
  field :order, 2, type: :string
  field :video_url, 3, type: :string
  field :image, 4, type: :string
end

defmodule Badgeapi.CreateBadgeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Badgeapi.UpdateBadgeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: String.t(),
          order: String.t(),
          id: String.t(),
          video_url: String.t(),
          image: String.t()
        }

  defstruct [:name, :order, :id, :video_url, :image]

  field :name, 1, type: :string
  field :order, 2, type: :string
  field :id, 3, type: :string
  field :video_url, 4, type: :string
  field :image, 5, type: :string
end

defmodule Badgeapi.UpdateBadgeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Badgeapi.GetBadgeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t()
        }

  defstruct [:id]

  field :id, 1, type: :string
end

defmodule Badgeapi.GetBadgeRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Badgeapi.GetBadgeResponseAll do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          badges: [Badgeapi.Badges.t()]
        }

  defstruct [:badges]

  field :badges, 1, repeated: true, type: Badgeapi.Badges
end

defmodule Badgeapi.Badges do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: String.t(),
          order: String.t(),
          id: String.t(),
          video_url: String.t(),
          image: String.t()
        }

  defstruct [:name, :order, :id, :video_url, :image]

  field :name, 1, type: :string
  field :order, 2, type: :string
  field :id, 3, type: :string
  field :video_url, 4, type: :string
  field :image, 5, type: :string
end

defmodule Badgeapi.DeleteBadgeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t()
        }

  defstruct [:id]

  field :id, 1, type: :string
end

defmodule Badgeapi.DeleteBadgeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end
defmodule Badgeapi.BadgeService.Service do
  @moduledoc false
  use GRPC.Service, name: "Badgeapi.BadgeService"

  rpc :CreateBadge, Badgeapi.CreateBadgeRequest, Badgeapi.CreateBadgeResponse

  rpc :GetBadge, Badgeapi.GetBadgeRequest, Badgeapi.GetBadgeResponseAll

  rpc :GetBadgeAll, Badgeapi.GetBadgeRequestAll, Badgeapi.GetBadgeResponseAll

  rpc :DeleteBadge, Badgeapi.DeleteBadgeRequest, Badgeapi.DeleteBadgeResponse

  rpc :UpdateBadge, Badgeapi.UpdateBadgeRequest, Badgeapi.UpdateBadgeResponse


end

defmodule Badgeapi.BadgeService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Badgeapi.BadgeService.Service
end
